import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

public abstract class Strategy implements Runnable {
	public static final Logger logger = Logger.getLogger("GLOBAL");
	// protected AtomicBoolean isLive;
	// isLive attribute represents the state before getting killed
	// run function will always have a format - while(isLive)
	protected List<Contract> contracts;
	protected double minSpread;
	protected double size;
	protected int count;
	protected int num;
	
	public abstract void hedge();
	public abstract void makeMoney();
	public abstract boolean getSignal();	
	
	public void run() {
		while(true) {
			hedge();
			makeMoney();
		}
	}
	
	public Strategy(List<Contract> cont, double minS, double s, int n) {
//		this.isLive = new AtomicBoolean();
		this.contracts = cont;
		this.minSpread = minS;
		this.size = s;
		this.num = n;
		this.count = 0;
	}
}

