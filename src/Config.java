import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {
	public static Properties prop;
	
	public static Properties getProperties() {
		if (prop == null) {
			prop = new Properties();
			InputStream input = null;
			try {
				input = new FileInputStream("config.properties");
				prop.load(input);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		return prop;
	}
	
	public static void main(String[] args) {
		Properties prop = Config.getProperties();
		
		System.out.println(prop.get("okexkey"));
	}
}
