
public class Contract {
	private Instrument instrument;
	private double orderQuantity;
	private double filledQuantity;
	private boolean direction;
	private boolean isAggressive;
	
	public Contract(Instrument inst, double quantity, boolean buyOrSell, boolean style) {
		this.instrument = inst;
		this.orderQuantity = quantity;
		this.filledQuantity = 0.0d;
		this.direction = buyOrSell;
		this.isAggressive = style;
	}
	
	public Instrument getInstrument() {
		return this.instrument;
	}
	public double getOrderQuantity() {
		return this.orderQuantity;
	}
	public double getDoneQuantity() {
		return this.filledQuantity;
	}
	public boolean getDirection() {
		return this.direction;
	}
	public boolean getIsAggressive() {
		return this.isAggressive;
	}
	
	public void setOrderQuantity(double newQuantity) {
		this.orderQuantity = newQuantity;
	}
	public void updateFilledQuantity(double newQuantity) {
		this.filledQuantity += newQuantity;
	}

	public double getMinSizeToShow() {
		return this.instrument.exchange.lotSizes.get(this.instrument.tradeCurrency);
	}
}
