import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

public final class Utils {
	private static final Logger logger = Logger.getLogger("GLOBAL");
	
	public static String dateToString(Date date) {
	    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	    return formatter.format(date);
	}
	
	public static Date stringToDate(String str) throws Exception{
	    Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(str); 
	    return date1;
	}

	public static void main(String[] args) throws Exception {
		logger.info(Utils.dateToString(new Date()));
		logger.info(Utils.stringToDate("24/6/2018"));
	}
	
	public static void printConcurrentHashMap( ConcurrentHashMap<String, List<Order>> map) {
		for (Entry<String, List<Order>> entry : map.entrySet()) {
		    String key = entry.getKey().toString();
		    List<Order> value = entry.getValue();
		    StringBuffer sb = new StringBuffer();
		    for(Order order : value) {
		    	sb.append(order.getPrice()+";");
		    }
		    logger.info("key, " + key + " value " + sb);
		}
	}
	

	public static void printConcurrentHashMap2(ConcurrentHashMap<String, Hashtable<String, Order>> map) {
		for (Entry<String, Hashtable<String, Order>> entry : map.entrySet()) {
		    String key = entry.getKey().toString();
		    Hashtable<String, Order> value = entry.getValue();
		    logger.info(key+"\n");
		    for (Entry<String, Order> ent : value.entrySet()) {
		    	String k = ent.getKey();
		    	String v = ent.getValue().toString();
		    	logger.info(k);
		    	logger.info(v);
		    }
		}
	}
	
    public static void setTimeout(Runnable runnable, int delay){
        new Thread(() -> {
            try {
                Thread.sleep(delay);
                runnable.run();
            }
            catch (Exception e){
                logger.error(e);
            }
        }).start();
    }
    
    public static void setTimeoutSync(Runnable runnable, int delay) {
        try {
            Thread.sleep(delay);
            runnable.run();
        }
        catch (Exception e){
            logger.error(e);
        }
    }
    
    public static HashMap<String, String> convertOrderBookL2ToHashTable(String msg) {
    	msg = msg.substring(1, msg.length()-1);           
    	String[] keyValuePairs = msg.split(",");            
    	HashMap<String, String> map = new HashMap<String, String>();               

    	for(String pair : keyValuePairs)
    	{
    	    String[] entry = pair.split(":");
    	    String k = entry[0].substring(1, entry[0].length()-1).trim();
    	    if (k.equals("table") || k.equals("action")) {
        	    String v = entry[1].substring(1, entry[1].length()-1).trim();
    	    	map.put(k, v);
    	    }
	    }
    	return map;
    }
    
    public static String tellBaseCurrency(String exchange, String tradeCurrency) throws Exception {
    	if(!exchange.equals("Bitmex"))	throw new Exception("wrong exchange");
    	if(tradeCurrency.startsWith("XBT"))
    		return "USD";
    	else
    		return "BTC";
    }

}
