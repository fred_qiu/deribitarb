import java.util.Calendar;
import java.util.Date;

public class Order {
	boolean direction; // buy = true, sell = false
	String baseCurrency;
	String tradeCurrency;
	double price;
	double quantity;
	String id;
	Date expiryDate;
	String expiry;
	boolean isLive = true;
	
	//Once Trade Object is instantiated, it should not be changed
	//So only get functions exist, no set function
	public Order(boolean myDirection, String myTradeCurrency, String myBaseCurrency, double myPrice, double myQuantity, String expiry) {
		this.direction = myDirection;
		this.baseCurrency = myBaseCurrency;
		this.tradeCurrency = myTradeCurrency;
		this.price = myPrice;
		this.quantity = myQuantity;
		this.expiry = expiry;
	}
	
	public Order(String id, boolean myDirection, String myTradeCurrency, String myBaseCurrency, double myPrice, double myQuantity) {
		this.id = id;
		this.direction = myDirection;
		this.baseCurrency = myBaseCurrency;
		this.tradeCurrency = myTradeCurrency;
		this.price = myPrice;
		this.quantity = myQuantity;
		this.expiry = expiry;
	}

	public Order(boolean myDirection, String myTradeCurrency, String myBaseCurrency, double myPrice, double myQuantity) {
		this(myDirection, myTradeCurrency, myBaseCurrency, myPrice, myQuantity, Utils.dateToString(Calendar.getInstance().getTime()));
	}	
	
	public Order(Order other) {
		this.direction = other.getDirection();
		this.baseCurrency = other.getBaseCurrency();
		this.tradeCurrency = other.getTradeCurrency();
		this.price = other.getPrice();
		this.quantity = other.getQuantity();
	}

	public boolean getDirection() {
		return direction;
	}
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public String getTradeCurrency() {
		return tradeCurrency;
	}
	public double getPrice() {
		return price;
	}
	public double getQuantity() {
		return quantity;
	}
	public String getID() {
		return id;
	}
	public String getExpiry() {
		return expiry;
	}
	
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setID(String myID) {
		id = myID;
	}
	public void setQuantity(double myQuantity) {
		quantity = myQuantity;
	}
	public void setDirection(boolean side) {
		direction = side;
	}
	public void setExpiryDate(Date date) {
		expiryDate = date;
	}
	public int comparePriceTo(Order order) {
		if (price > order.getPrice()) {
			return -1; 
		}
		else if (price < order.getPrice()) {
			return 1;
		}
		else {
			return 0;
		}
		
	}
	public void setDead() {
		this.isLive = false;
	}
	
	public boolean isExpired() {
		return this.expiryDate.before(new Date());
	}
	public String toString() {
		return "ID: " + id + ", direction: " + (direction ? "Buy" : "Sell") + ", baseCurrency: " + baseCurrency + ", tradeCurrency: " + tradeCurrency + ", price: " + price + ", quantity: " + quantity + ", expiry: " + expiry;
 	}
}
