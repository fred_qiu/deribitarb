import java.util.List;

public class ArbitrageStrategy extends Strategy {
	private Contract passiveContract;
	private Contract aggressiveContract;
	private Instrument passiveInstrument;
	private Instrument aggressiveInstrument;
	private Exchange passiveExchange;
	private Exchange aggressiveExchange;
	private String passiveTradeCurrency;
	private String passiveBaseCurrency;
	private String aggressiveTradeCurrency;
	private String aggressiveBaseCurrency;
	
	private Order passiveOrder;
	private boolean direction;
	private double currentBest;
	private double bestPriceToShow;
	
	public ArbitrageStrategy(List<Contract> cont, double minSpread, double size, int n) {
		super(cont, minSpread, size, n);
		
		for (Contract c : cont) {
			if (c.getIsAggressive()) {
				aggressiveContract = c;
				aggressiveInstrument = c.getInstrument();
				aggressiveExchange = aggressiveInstrument.getExchange();
				aggressiveTradeCurrency = aggressiveInstrument.getOldTradeCurrency();
				aggressiveBaseCurrency = aggressiveInstrument.getOldBaseCurrency();
			} else {
				passiveContract = c;
				passiveInstrument = c.getInstrument();
				passiveExchange = passiveInstrument.getExchange();
				passiveTradeCurrency = passiveInstrument.getOldTradeCurrency();
				passiveBaseCurrency = passiveInstrument.getOldBaseCurrency();
				direction = passiveContract.getDirection();
			}
		}
		
		logger.info("PassiveArbStrategy started: " + passiveInstrument.toFullString() + " vs. " + aggressiveInstrument.toFullString());
	}	
	
	
	public void makeMoney() {
		try {
			if(this.passiveOrder == null) {
				this.passiveOrder = passiveExchange.getBestBid(passiveInstrument.getOldTradeCurrency(), passiveInstrument.getOldBaseCurrency());
				this.passiveOrder.setQuantity(size);
				this.passiveOrder.setID(passiveExchange.execute(passiveOrder));
				logger.info("Now the best order to show is: " + passiveOrder.getPrice());
				
			} else {
				//check if the best; if not, cancel & improve to be the best next tick
				//real life is going to be litter more complicated... need to consider isOrderTight.
				if ((passiveOrder.getDirection() &&passiveOrder.getPrice() == passiveExchange.getBestBid(passiveInstrument.getOldTradeCurrency(), passiveInstrument.getOldBaseCurrency()).getPrice()) 
						|| (!passiveOrder.getDirection() && passiveOrder.getPrice() == passiveExchange.getBestOffer(passiveInstrument.getOldTradeCurrency(), passiveInstrument.getOldBaseCurrency()).getPrice())) {
						logger.info("Fine, we are still the best: " + passiveOrder.getPrice());
						Thread.sleep(1000);
				} else {
					passiveExchange.cancel(passiveOrder);
					passiveOrder = null;
					logger.info("cancelled the order because it's not the best anymore.");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public void hedge() {
		logger.info("hedge function starts...");
		if (passiveOrder != null) {
			try {
				double amountToHedge = passiveOrder.getQuantity() - passiveExchange.outstanding(passiveOrder);
				
				logger.info("Outstanding on Passive Order: " + passiveExchange.outstanding(passiveOrder));
				logger.info("Passive order: " + passiveOrder + " has been filled " + amountToHedge);
				
				if( amountToHedge > passiveOrder.getQuantity()) {
					// my bid wasn't sent to the exchange
					passiveExchange.cancel(passiveOrder);
					passiveOrder = null;
					logger.error("There is an issue with the passive Order: \n" + passiveExchange.getName());
					return;
				} else if(amountToHedge == 0) {
					logger.info("No hedge as nothing filled");
				} else {
					logger.info("amount to hedge: " + amountToHedge);
					Order hedgingOrder = direction ? 
							aggressiveExchange.CrossSpreadHit(aggressiveInstrument.getOldTradeCurrency(), aggressiveInstrument.getOldBaseCurrency()) :
							aggressiveExchange.CrossSpreadLift(aggressiveInstrument.getOldTradeCurrency(), aggressiveInstrument.getOldBaseCurrency());
					
					logger.info("Hedging Order: " + hedgingOrder);
					
					hedgingOrder.setQuantity(amountToHedge);
					hedgingOrder.setID(aggressiveExchange.execute(hedgingOrder));
					
					passiveExchange.updateBalance();
					aggressiveExchange.updateBalance();
					
					logger.info("Hedging completed");
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		} else {
			logger.info("nothing to hedge");
		}
		
		return;
	}


	@Override
	public boolean getSignal() {
		// This function was supposed to be used for red-light / green-light ongoing process but not applicable in this case.
		return false;
	}
	
	
	
}
