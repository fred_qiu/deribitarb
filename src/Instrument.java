import org.apache.log4j.Logger;

public class Instrument {
	/*
	 * [XBT, BTC] -> BTC, [] -> LTC
	 * Futures: F: January; G: February; H: March; J: April; K: May; M: June; N: July; Q:August;U: September; V: October; X: November; Z: December;
	 * Both perpetual swap and cash are as 0.
	 */
	private static final Logger logger = Logger.getLogger("GLOBAL");
	
	Exchange exchange;
	String tradeCurrency;
	String baseCurrency;
	String expiryDate;
	String oldTradeCurrency;
	String oldBaseCurrency;
	String type; // cash or future or margin
	
	Instrument(Exchange exchange, String tradeCurrency, String baseCurrency, String expiryDate, String instrType){
		this(exchange, tradeCurrency, baseCurrency, expiryDate, instrType, tradeCurrency);
	}
	
	Instrument(Exchange exchange, String tradeCurrency, String baseCurrency, String expiryDate, String instrType, String oldTradeCurrency){
		this(exchange, tradeCurrency, baseCurrency, expiryDate, instrType, oldTradeCurrency, baseCurrency);
	}
	
	Instrument(Exchange exchange, String tradeCurrency, String baseCurrency, String expiryDate, String instrType, String oldTradeCurrency, String oldBaseCurrency){
		this.exchange = exchange;
		this.tradeCurrency = tradeCurrency;
		this.baseCurrency = baseCurrency;
		this.expiryDate = expiryDate;
		this.type = instrType;
		this.oldTradeCurrency = oldTradeCurrency;
		this.oldBaseCurrency = oldBaseCurrency;
	}
	
	public Exchange getExchange() {
		return exchange;
	}
	
	public void setExchange(Exchange exchange) {
		this.exchange = exchange;
	}


	public String getTradeCurrency() {
		return tradeCurrency;
	}
	
	public String getOldTradeCurrency() {
		return oldTradeCurrency;
	}

	public String getOldBaseCurrency() {
		return oldBaseCurrency;
	}


	public String getBaseCurrency() {
		return baseCurrency;
	}


	public String getTimeToExpiry() {
		return expiryDate;
	}
	
	public String getType() {
		return type;
	}
	
	private Order emptyBid() {
		return new Order(true, this.getTradeCurrency(), this.getBaseCurrency(), -1d, 0d);
	}
	public Order getBestBid() {
		if (this.exchange.getBestBid(this.oldTradeCurrency, this.oldBaseCurrency) == null) {
			return new Order(true, this.getTradeCurrency(), this.getBaseCurrency(), -1d, 0d);
		}
		return this.exchange.getBestBid(this.oldTradeCurrency, this.oldBaseCurrency);
	}
	public Order getBestOffer() {
		if (this.exchange.getBestOffer(this.oldTradeCurrency, this.oldBaseCurrency) == null) {
			return new Order(false, this.getTradeCurrency(), this.getBaseCurrency(), -1d, 0d);
		}
		return this.exchange.getBestOffer(this.oldTradeCurrency, this.oldBaseCurrency);
	}
	public long getTimeStamp() {
		return this.exchange.getTimeStamps_Price(this.oldTradeCurrency, this.oldBaseCurrency);
	}
	
	public double getDelta(String currency) {
		if (currency.equals(this.tradeCurrency)) {
			if (!this.exchange.balances.containsKey(this.oldTradeCurrency))	return 0;
			return this.exchange.getBalance(this.oldTradeCurrency).getTotalBalance();
			/*
			logger.info(this.getExchange());
			logger.info("Trade currency: " + tradeCurrency);
			logger.info("Old Trade Currency: " + oldTradeCurrency);
			logger.info(this.exchange.getBalance(this.oldTradeCurrency).getTotalBalance());
			return 0;
			*/
		}
		if (!getType().equals("cash") && !getType().equals("margin") && currency.equals(this.baseCurrency)) {
			double tradeDelta = this.exchange.getBalance(this.oldTradeCurrency).getTotalBalance();
			// General way of calculating the trade price.
			// Bitmex generated mark price may be different
			// Which way of price marking should be used? To be decided
			double tradePrice = (this.getBestBid().getPrice() + this.getBestOffer().getPrice())/2;
			return -tradeDelta*tradePrice;
		}
		return 0;
	}
	
//	public double getDeltaNU(CRB crb, String currency) {
//		return this.getDelta(currency) * crb.getTV(new Instrument(this.exchange, currency, "USD", LocalDate.now(), "cash"));
//	}
	
	public String toString() {
		return this.exchange.getName() + this.tradeCurrency + this.baseCurrency + this.expiryDate.toString() + this.getType();
	}
	
	public String toFullString() {
		return this.exchange.getName() + this.tradeCurrency + this.baseCurrency + this.expiryDate.toString() + this.getType() + this.oldTradeCurrency + this.oldBaseCurrency;
	}
	
//	public int getNumOfDaysFromExpiry() {
//		return this.exchange.expirydateToDays(this.getTimeToExpiry());
//	}
}