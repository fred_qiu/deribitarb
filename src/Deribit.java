import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

//future
public class Deribit extends Exchange {
	private static final Logger logger = Logger.getLogger("GLOBAL");
	SimpleDateFormat dateForm = new SimpleDateFormat("ddMMMyy");

	public Deribit() {
		Properties prop = Config.getProperties();
		
		name = "Deribit";
		publicKey = prop.getProperty("deribitkey");
		privateKey = prop.getProperty("deribitsecret");
		baseAddress = "https://test.deribit.com/api/v1/";
		takerFee = 0.0005;
		makerFee = -0.0002;
		baseFiatCurrency = "BTC";

		ticks.putIfAbsent("BTC-28JUN19USD", 0.1);
		ticks.putIfAbsent("BTC-27SEP19USD", 0.1);
		ticks.putIfAbsent("BTC-PERPETUALUSD", 0.1);
				
		//Lot size for each currency pair
		lotSizes.putIfAbsent("BTC-28JUN19USD", 0.1);
		lotSizes.putIfAbsent("BTC-27SEP19USD", 0.1);
		lotSizes.putIfAbsent("BTC-PERPETUALUSD", 0.1);
				
		timeStamps_Price.putIfAbsent("BTC-28JUN19USD", 0l);
		timeStamps_Price.putIfAbsent("BTC-27SEP19USD", 0l);
		timeStamps_Price.putIfAbsent("BTC-PERPETUALUSD", 0l);
	}

	class ResultBody {
		private OrderBody order;
		public OrderBody getOrder() {
			return order;
		}
	}

	class ResultBody2 {
		private double quantity;
		private double filledQuantity;
		public double getQuantity() {
			return quantity;
		}
		public double getfilledQuantity() {
			return filledQuantity;
		}
	}

	class ResultBody3 {
		private double balance;
		public double getBalance() {
			return balance;
		}
	}

	class OrderBook {
		ArrayList<Order> bids;
		ArrayList<Order> asks;
		public double getXthBidPrice(int x) {
			return bids.get(x).getPrice();
		}
		public double getXthBidQuantity(int x) {
			return bids.get(x).getQuantity();
		}
		public double getXthAskPrice(int x) {
			return asks.get(x).getPrice();
		}
		public double getXthAskQuantity(int x) {
			return asks.get(x).getQuantity();
		}
	}

	class OrderBody {
		private String orderId;
		public String getID() {
			return orderId;
		}
	}
	
	class ResponseBody {
		private boolean success;
		private ResultBody result;
		private String message;
		public boolean getSuccess() {
			return success;
		}
		public ResultBody getResult() {
			return result;
		}
		public String getMessage() {
			return message;
		}
	}

	class ResponseBody2 {
		private boolean success;
		private ResultBody result;
		private String message;
		private OrderBody order;
		public boolean getSuccess() {
			return success;
		}
		public ResultBody getResult() {
			return result;
		}
		public String getMessage() {
			return message;
		}
		public OrderBody getOrder() {
			return order;
		}
	}

	class ResponseBody3 {
		private boolean success;
		private ResultBody2 result;
		private int msIn;
		private int msOut;
		public boolean getSuccess() {
			return success;
		}
		public ResultBody2 getResult() {
			return result;
		}
		public int getMsIn() {
			return msIn;
		}
		public int getMsOut() {
			return msOut;
		}
	}

	class ResponseBody4 {
		private boolean success;
		private OrderBook result;
		private String message;
		public boolean getSuccess() {
			return success;
		}
		public OrderBook getResult() {
			return result;
		}
		public String getMessage() {
			return message;
		}
	}

	class ResponseBody5 {
		private boolean success;
		private ResultBody3 result;
		public boolean getSuccess() {
			return success;
		}
		public ResultBody3 getResult() {
			return result;
		}
	}

	@Override
	public String execute(Order order) throws ClientProtocolException, IOException, JSONException {
		if (order.getQuantity()==0) { return "No execution: quantity = 0"; }
		long nonce = (new Date()).getTime();
		String postData = "";
		String message = "_=" + nonce + "&_ackey=" + publicKey + "&_acsec=" + privateKey + "&_action=" + "/api/v1/private/" + (order.getDirection() ? "buy" : "sell");
		message += "&instrument=" + order.getTradeCurrency();
		message += "&post_only=true";
		message += "&price=" + String.format("%1.2f", order.getPrice());
		message += "&quantity=" + String.format("%1.2f", order.getQuantity());
		message += "&type=limit";

		String signature = publicKey + "." + nonce + "." + CreateToken(message);
		JSONObject json = new JSONObject();
		String inst = order.getTradeCurrency();
		json.put("instrument", inst);
		json.put("post_only", "true");
		json.put("price", String.format("%1.2f", order.getPrice()));
		json.put("quantity", String.format("%1.2f", order.getQuantity()));
		json.put("type", "limit");

		postData = json.toString();
		Entity<String> payload = Entity.json(postData);

		Client client = ClientBuilder.newClient();
		Response response = client.target(baseAddress + "private/" + (order.getDirection() ? "buy" : "sell"))
		  .request(MediaType.APPLICATION_JSON_TYPE)
		  .header("Content-Type", "application/x-www-form-urlencoded")
		  .header("x-deribit-sig", signature)
		  .post(payload);

		String body = response.readEntity(String.class);
		Gson gson = new Gson();
		ResponseBody responseJSon = gson.fromJson(body, ResponseBody.class);
		try {
			order.setID(responseJSon.getResult().getOrder().getID());
			logger.info(order.toString() + " has been executed in " + this.getName());
			return responseJSon.getResult().getOrder().getID();
		} catch (Exception e) {
			logger.error("Failed to execute order " + order.toString() + " in " + this.getName());
			logger.error("Response body: " + body);
			logger.error(message);
			logger.error(json);
			return null;
		}
	}

	@Override
	public boolean cancel(Order order) throws ClientProtocolException, IOException, JSONException {
		long nonce = (new Date()).getTime();
		String message = "_=" + nonce + "&_ackey=" + publicKey + "&_acsec=" + privateKey + "&_action=" + "/api/v1/private/cancel";
		message += "&orderId=" + order.getID();
		String postData = "";

		String signature = publicKey + "." + nonce + "." + CreateToken(message);
		JSONObject json = new JSONObject();
		json.put("orderId", order.getID());
		postData = json.toString();
		Entity<String> payload = Entity.json(postData);

		Client client = ClientBuilder.newClient();
		Response response = client.target(baseAddress + "private/cancel")
		  .request(MediaType.APPLICATION_JSON_TYPE)
		  .header("Content-Type", "application/x-www-form-urlencoded")
		  .header("x-deribit-sig", signature)
		  .post(payload);

		String body = response.readEntity(String.class);
		Gson gson = new Gson();
		try {
			ResponseBody2 responseJSon = gson.fromJson(body, ResponseBody2.class);
			Boolean isSuccess = responseJSon.getSuccess();
			if(isSuccess) {
				logger.info(order.toString() + " has been cancelled in " + this.getName());
			}
			return isSuccess;
		} catch (Exception e) {
			logger.error("Cancellation of " + order.toString() + "failed in " + this.getName() + ". for " + e);
			logger.error("Response body: " + body);
			return false;
		}
	}

	@Override
	public double outstanding(Order order) throws ClientProtocolException, IOException {
		long nonce = (new Date()).getTime();
		String message = "_=" + nonce + "&_ackey=" + publicKey + "&_acsec=" + privateKey + "&_action=" + "/api/v1/private/orderstate";
		message += "&orderId=" + order.getID();

		String signature = publicKey + "." + nonce + "." + CreateToken(message);

		Client client = ClientBuilder.newClient();
		Response response = client.target(baseAddress + "private/orderstate?orderId=" + order.getID())
		  .request(MediaType.APPLICATION_JSON_TYPE)
		  .header("Content-Type", "application/x-www-form-urlencoded")
		  .header("x-deribit-sig", signature)
		  .get();

		String body = response.readEntity(String.class);
		Gson gson = new Gson();
		ResponseBody3 responseJSon = gson.fromJson(body, ResponseBody3.class);
		try {
			return (responseJSon.getResult().getQuantity() - responseJSon.getResult().getfilledQuantity());
		} catch (Exception e) {
			logger.error("Fail to get outstanding info in " + this.getName() + " response code is " + e);
			logger.error("Response body: " + body);
			return -1;
		}
	}

	
	@Override
	public boolean readOrderBook(String tradeCurrency, String baseCurrency) throws IOException {
		Client client = ClientBuilder.newClient();
		Response response = client.target(baseAddress + "public/getorderbook?instrument=" + tradeCurrency)
		  .request(MediaType.APPLICATION_JSON_TYPE)
		  .header("Content-Type", "application/x-www-form-urlencoded")
		  .get();

		String body = response.readEntity(String.class);
		Gson gson = new Gson();
		OrderBook orderbook = gson.fromJson(body, ResponseBody4.class).getResult();

		try {
			List<Order> bids = new ArrayList<Order>();
			List<Order> offers = new ArrayList<Order>();
			for (int i=0; i<10; i++) {
				Order bid = new Order(true, tradeCurrency, baseCurrency, orderbook.getXthBidPrice(i), orderbook.getXthBidQuantity(i));
				Order offer = new Order(false, tradeCurrency, baseCurrency, orderbook.getXthAskPrice(i), orderbook.getXthAskQuantity(i));
				bids.add(i,bid);
				offers.add(i,offer);
			}
			bestBids.put(tradeCurrency+baseCurrency,bids);
			bestOffers.put(tradeCurrency+baseCurrency,offers);
			timeStamps_Price.put(tradeCurrency+baseCurrency, System.currentTimeMillis());
			return true;
		}
		catch (NullPointerException e) {
			logger.error("Error in readingOrderBook in " + this.getName() + tradeCurrency + ": " + e);
			logger.error("Response body: " + body);
			return false;
		}
	}
	
	@Override
	public boolean readBalance() throws IOException {
		long nonce = (new Date()).getTime();
		String message = "_=" + nonce + "&_ackey=" + publicKey + "&_acsec=" + privateKey + "&_action=" + "/api/v1/private/account";

		double balance = 0.0;
		String signature = publicKey + "." + nonce + "." + CreateToken(message);
		Client client = ClientBuilder.newClient();
		Response response = client.target(baseAddress + "private/account")
		  .request(MediaType.APPLICATION_JSON_TYPE)
		  .header("Content-Type", "application/x-www-form-urlencoded")
		  .header("x-deribit-sig", signature)
		  .get();

		String body = response.readEntity(String.class);
		Gson gson = new Gson();
		ResponseBody5 responseJSon = gson.fromJson(body, ResponseBody5.class);
		balance = responseJSon.getResult().getBalance();
		try {
			balances.put("BTC", new Balance("BTC", balance, balance));
			return true;
		}
		catch (NullPointerException e) {
			logger.error("Failed to read position in balance of " + getName() + " for " + e);
			logger.error("Response body: " + body);
			return false;
		}
	}

	private static String CreateToken(String message){
		String hash = "";
		try {    
			MessageDigest digest = MessageDigest.getInstance("SHA-256"); 
			byte[] encodedhash = digest.digest( message.getBytes(StandardCharsets.UTF_8));
			String signature = Base64.encodeBase64String(encodedhash);
			return signature;
		}
		catch (Exception e){
			logger.error("Error creating token in Deribit for " + e);
		}
		return hash;
	}
		
	public static void main(String args[]) throws IOException {
		Deribit deribit = new Deribit();
		Order order = new Order(true, "BTC-28JUN19", "USD", 10745.00, 1.00);
		try {
			String id = deribit.execute(order);
			logger.info(id + " is just placed");
			logger.info(deribit.outstanding(order) + " is outstanding");
			logger.info("cancellation: " + deribit.cancel(order));
		} catch (Exception e) {
			logger.error("Stack Trace", e);
		}
	}
}
