import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.json.JSONException;

import java.time.temporal.ChronoUnit;

public abstract class Exchange {
	private static final Logger logger = Logger.getLogger("GLOBAL");
	protected String name;
	protected String publicKey;
	protected String privateKey;
	protected String baseAddress;
	protected String baseFiatCurrency;
	protected ConcurrentHashMap<String, Balance> balances;
	//String key = currency_pair = TradeCurrency + BaseCurrency
	protected ConcurrentHashMap<String, Double> ticks;
	protected ConcurrentHashMap<String, Double> lotSizes;
	protected ConcurrentHashMap<String, List<Order>> bestBids;
	protected ConcurrentHashMap<String, List<Order>> bestOffers;
	protected ConcurrentHashMap<String, Long> timeStamps_Price;
	protected ConcurrentHashMap<String, Long> timeStamps_Balance;
	protected ConcurrentHashMap<String, LocalDate> futureExpiries;
	protected double takerFee;
	protected double makerFee;
	
	
	
	public Exchange() {
		//Constructor
		balances = new ConcurrentHashMap<String, Balance>();
		ticks = new ConcurrentHashMap<String, Double>();
		lotSizes = new ConcurrentHashMap<String, Double>();
		bestBids = new ConcurrentHashMap<String, List<Order>>();
		bestOffers = new ConcurrentHashMap<String, List<Order>>();
		timeStamps_Price = new ConcurrentHashMap<String, Long>();
		futureExpiries = new ConcurrentHashMap<String, LocalDate>();
	}
	
	abstract public String execute(Order order) throws ClientProtocolException, IOException, JSONException;
	abstract public boolean cancel(Order order) throws ClientProtocolException, IOException, JSONException;
	abstract public double outstanding(Order order) throws ClientProtocolException, IOException;
	abstract public boolean readOrderBook(String tradeCurrency, String baseCurrency) throws IOException;
	abstract public boolean readBalance() throws IOException;
//	abstract public List<Instrument> getAllInstruments();
	
	public void updateOrderBook(String tradeCurrency, String baseCurrency) {
		final int MAX_READ_BALANCE = 5;
		for (int i=0; i<MAX_READ_BALANCE; i++) {
			try {
				if (readOrderBook(tradeCurrency, baseCurrency)) {
					timeStamps_Price.put(tradeCurrency+baseCurrency, System.currentTimeMillis());
					break;
				}
			} catch (IOException e1) {
				System.err.println("Exception caught in " + new Object(){}.getClass().getEnclosingMethod().getName());
				e1.printStackTrace();
			}
		}
	}
	
	public void updateBalance() {
		final int MAX_READ_BALANCE = 5;
		for (int i=0; i<MAX_READ_BALANCE; i++) {
			try {
				if (readBalance()) {
					break;
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("Had an issue with updating a balance of " + this.getName());
			try {
				Thread.sleep(1000);
			}
			catch(InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
	
	public String getName() {
		return name;
	}
	public double effectivePrice(Order order) {
		if (order.getDirection()) {
			if (getBestOffer(order.getTradeCurrency(), order.getBaseCurrency()).getPrice() <= order.getPrice()) {
				return order.getPrice() * (1.0+takerFee);
			}
			else {
				return order.getPrice() * (1.0+makerFee);
			}		
		}
		else {
			if (getBestBid(order.getTradeCurrency(), order.getBaseCurrency()).getPrice() >= order.getPrice()) {
				return order.getPrice() * (1.0-takerFee);
			}
			else {
				return order.getPrice() * (1.0-makerFee);
			}
		}
	}
	public Balance getBalance(String currency) {
		//System.out.println(balances.toString());
		if (balances.containsKey(currency)) {
			return new Balance(balances.get(currency));
		}
		return new Balance(currency, 0d, 0d);
	}
	public Order getXthBestBid(String tradeCurrency, String baseCurrency, int X) {
		if (bestBids.containsKey(tradeCurrency+baseCurrency)) {
			return new Order(bestBids.get(tradeCurrency+baseCurrency).get(X));
		}
		return null;
	}
	public Order getBestBid(String tradeCurrency, String baseCurrency) {
		return getXthBestBid(tradeCurrency, baseCurrency, 0);
	}
	public Order getXthBestOffer(String tradeCurrency, String baseCurrency, int X) {
		if (bestOffers.containsKey(tradeCurrency+baseCurrency)) {
			return new Order(bestOffers.get(tradeCurrency+baseCurrency).get(X));
		}
		return null;
	}
	public Order getBestOffer(String tradeCurrency, String baseCurrency) {
		return getXthBestOffer(tradeCurrency, baseCurrency, 0);
	}
	public double getBidPriceAtQuantity(String tradeCurrency, String baseCurrency, double quantity) {
		double cumQuantity = 0;
		for (int i=0; i<10; i++) {
			Order theBid = getXthBestBid(tradeCurrency, baseCurrency, i);
			cumQuantity += theBid.getQuantity();
			if (cumQuantity >= quantity) {
				return theBid.getPrice();
			}
		}
		return 0; //Min Bid
	}
	public double getAskPriceAtQuantity(String tradeCurrency, String baseCurrency, double quantity) {
		double cumQuantity = 0;
		for (int i=0; i<10; i++) {
			Order theOffer = getXthBestOffer(tradeCurrency, baseCurrency, i);
			cumQuantity += theOffer.getQuantity();
			if (cumQuantity >= quantity) {
				return theOffer.getPrice();
			}
		}
		return Double.MAX_VALUE;
	}
	public double getTick(String tradeCurrency, String baseCurrency) {
		return ticks.get(tradeCurrency+baseCurrency);
	}
	public long getTimeStamps_Price(String tradeCurrency, String baseCurrency) {
		if (timeStamps_Price.containsKey(tradeCurrency+baseCurrency)) {
			return timeStamps_Price.get(tradeCurrency+baseCurrency);
		}
		return -1l;
	}
	public double roundUpPrice(double price, String tradeCurrency, String baseCurrency) {
		double tick = getTick(tradeCurrency, baseCurrency);
		return tick*(Math.ceil(price/tick));
	}
	public double roundDownPrice(double price, String tradeCurrency, String baseCurrency) {
		double tick = getTick(tradeCurrency, baseCurrency);
		return tick*(Math.floor(price/tick));
	}
	public double getLotSize(String tradeCurrency, String baseCurrency) {
		System.out.println(tradeCurrency);
		System.out.println(baseCurrency);
		return lotSizes.get(tradeCurrency+baseCurrency);
	}
	public double roundLotSize(double quantity, String tradeCurrency, String baseCurrency) {
		double lotSize = getLotSize(tradeCurrency, baseCurrency);
		double rounded = lotSize*(Math.round(quantity/lotSize));
		BigDecimal bd = new BigDecimal(rounded).setScale(-(int)(Math.log10(lotSize)), RoundingMode.HALF_EVEN);
		return bd.doubleValue();
	}
		
	public Order hit(String tradeCurrency, String baseCurrency) {
		Order t = getBestBid(tradeCurrency, baseCurrency);
		return new Order(!t.getDirection(), t.getTradeCurrency(), t.getBaseCurrency(), t.getPrice(), t.getQuantity());
	}
	
	public Order CrossSpreadHit(String tradeCurrency, String baseCurrency) {
		Order t = getBestOffer(tradeCurrency, baseCurrency);
		return new Order(!t.getDirection(), t.getTradeCurrency(), t.getBaseCurrency(), t.getPrice(), t.getQuantity());
	}
	
	public Order lift(String tradeCurrency, String baseCurrency) {
		Order t = getBestOffer(tradeCurrency, baseCurrency);
		return new Order(!t.getDirection(), t.getTradeCurrency(), t.getBaseCurrency(), t.getPrice(), t.getQuantity());
	}
	
	public Order CrossSpreadLift(String tradeCurrency, String baseCurrency) {
		Order t = getBestBid(tradeCurrency, baseCurrency);
		return new Order(!t.getDirection(), t.getTradeCurrency(), t.getBaseCurrency(), t.getPrice(), t.getQuantity());
	}
	
	public double getTakerFee() {
		return takerFee;
	}
	public double getMakerFee() {
		return makerFee;
	}
	
	public Order improvePriceOneTick(Order order) {
		return improvePriceXTick(order, 1);
	}
	public Order improvePriceXTick(Order order, int x) {
		
		double tick = getTick(order.getTradeCurrency(), order.getBaseCurrency());
		double newPrice = order.getPrice();
		if (order.getDirection()) {
			newPrice += tick*x;
		}
		else {
			newPrice -= tick*x;
		}
		return new Order(order.getDirection(), order.getTradeCurrency(), order.getBaseCurrency(), newPrice, order.getQuantity());
	}
	public boolean isOrderbookTight(String tradeCurrency, String baseCurrency) {
		logger.info("Is Orderbook Tight for: " + tradeCurrency +  "/" + baseCurrency);
		logger.info("Best bid: " + hit(tradeCurrency, baseCurrency).getPrice());
		logger.info("Best offer: " + lift(tradeCurrency, baseCurrency).getPrice());
		logger.info("Tick: " + getTick(tradeCurrency, baseCurrency));
		if (hit(tradeCurrency, baseCurrency).getPrice() + getTick(tradeCurrency, baseCurrency) >= lift(tradeCurrency, baseCurrency).getPrice()) {
			logger.info("Orderbook is tight");
			return true;
		}
		return false;
	}
	public String getBaseFiatCurrency() {
		return baseFiatCurrency;
	}
	public int expirydateToDays(LocalDate expiry) {
		return (int) ChronoUnit.DAYS.between(expiry, LocalDate.now());
	}
}
