/**
* The Driver.java is the entrance of DeribitArb program, which implements an application that
* arbitrages between future M19 and U19.
*
* This version derives from my own codebase of "arbitrgeurs" therefore I continue using V1 APIs for
* Deribit.
*
* @author  Fred Qiu
* @version 1.0
* @since   2019-06-24 
*/

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.log4j.Logger;

public class Driver {
	private static final Logger logger = Logger.getLogger("GLOBAL");
	
	static class ReadInPrices implements Runnable {
		private final static int NUM_ITERATION = 60*60*10;
		private Exchange exchange;
		private String oldTradeCurrency;
		private String oldBaseCurrency;
		private int frequencyPerMin;
		public ReadInPrices(Exchange ex, String oldTradeCurrency, String oldBaseCurrency, int frequency) {
			this.exchange = ex;
			this.oldTradeCurrency = oldTradeCurrency;
			this.oldBaseCurrency = oldBaseCurrency;
			this.frequencyPerMin = frequency;
		}
		public void run() {
			try {
				for (int i = 0; i < NUM_ITERATION; i++) {
					exchange.updateOrderBook(oldTradeCurrency, oldBaseCurrency);
					Thread.sleep(1000*60/frequencyPerMin);
				}
			}
			catch (InterruptedException e) {
				System.out.println("Exception: market depth reading interrupted. Restart");
				try {
					Thread.sleep(1000*3);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				this.run();
			}
		}
	}
	
	
	public static void main(String args[]) {
		// below variables should be stored in a map but much simpler way for one-exchange case.
		final int freq = 200 * 60;
		final int numOfInstruments = 2;
		
		try {
			Exchange deribit = new Deribit();
			Thread readInPriceJune = new Thread(new ReadInPrices(deribit, "BTC-28JUN19", "USD", freq / numOfInstruments));
			Thread readInPriceSep= new Thread(new ReadInPrices(deribit, "BTC-27SEP19", "USD", freq / numOfInstruments));
			readInPriceJune.start();
			readInPriceSep.start();
			Thread.sleep(5000);		// wait for feed kicks in
			
			double juneSpread = 0;
			double septSpread = 0;
			double midJune = 0;
			double midSept = 0;
			Instrument juneFuture = null;
			Instrument septFuture = null;
			while(true) {
				juneFuture = new Instrument(deribit, "BTC", "USD", "28JUN19", "future", "BTC-28JUN19", "USD");
				septFuture = new Instrument(deribit, "BTC", "USD", "27SEP19", "future", "BTC-27SEP19", "USD");
			
				juneSpread = juneFuture.getBestOffer().getPrice() - juneFuture.getBestBid().getPrice();
				septSpread = septFuture.getBestOffer().getPrice() - septFuture.getBestBid().getPrice();
				midJune = (juneFuture.getBestBid().getPrice() + juneFuture.getBestOffer().getPrice()) / 2;
				midSept = (septFuture.getBestBid().getPrice() + septFuture.getBestOffer().getPrice()) / 2;
				if(midJune != midSept && juneSpread != septSpread) {
					// we started an arb only when there is discrepancy of mid price and spread
					logger.info("arb opportunities captured...spread: " + juneSpread + " vs. " + septSpread);

					Contract juneContract = new Contract(juneFuture, 0.001, midJune < midSept, juneSpread < septSpread);
					Contract septContract = new Contract(septFuture, 0.001, midJune > midSept, juneSpread > septSpread);
					Contract[] contracts = {juneContract, septContract};
					Strategy strat = new ArbitrageStrategy(new ArrayList<Contract>(Arrays.asList(contracts)), 0.002, 2, 1000);
					strat.run();
				} else {
					logger.info("waiting for spread diff between instruments");
					Thread.sleep(2000);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}


