import org.apache.log4j.Logger;

public class Balance {
	private static final Logger logger = Logger.getLogger("GLOBAL");
	private String currency;
	private double totalBalance;
	private double availableBalance;
	//pendingIncoming, pendingOutgoing, openOrder
	
	public Balance(String cncy, double total, double avail) {
		currency = cncy;
		totalBalance = total;
		availableBalance = avail;
	}
	public Balance(Balance other) {
		currency = other.getCurrency();
		totalBalance = other.getTotalBalance();
		availableBalance = other.getAvailableBalance();
	}
	
	public String getCurrency() {
		return currency;
	}
	public double getTotalBalance() {
		return totalBalance;
	}
	public double getAvailableBalance() {
		return availableBalance;
	}
	public String toString() {
		return "Currency: " + currency +", " + "Total Balance: " + totalBalance + ", " + "Available Balance: " + availableBalance + "\n";
	}
}
